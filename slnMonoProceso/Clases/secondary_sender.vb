﻿Public Class secondary_sender

    Public Property document_type As String
    Public Property document_number As String
    Public Property verification_digit As String
    Public Property person_type As String
    Public Property social_reason As String
    Public Property first_name As String
    Public Property last_name As String
    Public Property commercial_name As String
    Public Property initials As String
    Public Property tributary_condition As String
    Public Property statal_entity As String
    Public Property consortium As String
    Public Property invoices_printer_nit As String
    Public Property invoices_printer_social_reason As String
    Public Property aggregation_code As String
    Public Property phone As String
    Public Property email As String
    Public Property address As String

    Public Property city_code As String
    Public Property country_code As String
    Public Property use_incentives As String


End Class
