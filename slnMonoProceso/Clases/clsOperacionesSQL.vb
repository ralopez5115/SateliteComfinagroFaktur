﻿Imports System.Data.SqlClient
Imports System.Data.OracleClient

Public Class clsOperacionesSQL

    Public Function ConsultarFacturas() As DataSet

        Dim SqlConnection As New SqlConnection(My.Settings.strConexionGT)

        SqlConnection.Open()

        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand("SELECT   distinct

		                            f350_id_co co 
					                ,f350_id_tipo_docto tipo
                                    ,f350_consec_docto nro
                                    ,dbo.t350_co_docto_contable.f350_id_tipo_docto + '-' + RIGHT('00000000' + CONVERT(varchar(50), dbo.t350_co_docto_contable.f350_consec_docto), 8) as operation_id

                                    ,		'V'																					AS operation_type
                                    ,		CONVERT(nvarchar(10), dbo.t461_cm_docto_factura_venta.f461_id_fecha, 23) 			AS operation_issue_date
                                    ,		CONVERT(nvarchar(10), dbo.t461_cm_docto_factura_venta.f461_id_fecha, 23) 			AS operation_filing_date
                                    ,		CONVERT(nvarchar(10), dbo.t461_cm_docto_factura_venta.f461_id_fecha, 23) 			AS operation_posting_date
                                    ,		CONVERT(nvarchar(10), dbo.t353_co_saldo_abierto.f353_fecha_vcto, 23) 				AS operation_expiration_date
                                    ,		dbo.t350_co_docto_contable.f350_id_moneda_docto										AS currency
                                    ,		dbo.t010_mm_companias.f010_nit					AS main_sender  

                                    ,		CASE WHEN dbo.t200_mm_terceros.f200_id_tipo_ident in ('N') THEN 'A' ELSE 'C' END  AS document_type
		
					                ,		CASE WHEN dbo.t200_mm_terceros.f200_id_tipo_ident in ('N') THEN dbo.t200_mm_terceros.f200_nit  ELSE dbo.t200_mm_terceros.f200_id END  AS document_number

					                ,		CASE WHEN dbo.t200_mm_terceros.f200_id_tipo_ident in ('N','R') THEN dbo.t200_mm_terceros.f200_dv_nit  ELSE '' END AS verification_digit

                                    ,       case when dbo.t200_mm_terceros.f200_id_tipo_ident in ('N') then 'J' else 'N' end as person_type
                                    ,		case when dbo.t200_mm_terceros.f200_id_tipo_ident in ('N') then  dbo.t200_mm_terceros.f200_razon_social	else '' end		AS social_reason
                                    ,		dbo.t200_mm_terceros.f200_nombres				AS first_name

                                    --,		CASE dbo.t200_mm_terceros.f200_ind_tipo_tercero WHEN 1 THEN dbo.t200_mm_terceros.f200_nombre_est ELSE '' END AS OtrosNombresMdteSec--**

                                    ,		dbo.t200_mm_terceros.f200_apellido1				AS last_name
                                    --,		dbo.t200_mm_terceros.f200_apellido2				AS SegundoApellidoMdteSec--**
                                    ,		dbo.t200_mm_terceros.f200_nombre_est			AS commercial_name
					                ,		substring(case when dbo.t200_mm_terceros.f200_razon_social = '' then dbo.t200_mm_terceros.f200_nombres else dbo.t200_mm_terceros.f200_razon_social end ,0,2)	AS initials
                                    ,		'CN'											AS tributary_condition -- VALIDAR CON CLIENTE
                                    ,		'False'											AS statal_entity --VALIDAR CON CLIENTE DE DONDE TOMAR ESTE DATO S ENTIDAD ESTATAL N NO ES ENTIDAD ESTATAL
                                    ,		'False'											AS consortium --VALIDAR DE DONDE TOMAR. S CONSORCIO N NO ES UN CONSORCIO
                                    --,		''												AS FactPreImpresaMdteSec --**--VALIDAR DE DONDE TOMAR. S MANEJA FAC PREIMPRESA N NO MANEJA
                                    ,		''												AS invoices_printer_nit --VALIDAR CON CLIENTE
                                    ,		''												AS invoices_printer_social_reason --VALIDAR CON CLIENTE
                                    ,		3												AS aggregation_code    --FIJO 3 = FENAVI
                                    ,		dbo.t015_mm_contactos.f015_telefono				AS phone
					                ,		substring(dbo.t015_mm_contactos.f015_email,0,charindex(';',dbo.t015_mm_contactos.f015_email))				AS email
                                    ,		'' AS address --dbo.t015_mm_contactos.f015_direccion1			AS address

                                    ,		dbo.t015_mm_contactos.f015_id_depto +  dbo.t015_mm_contactos.f015_id_ciudad								AS city_code

                                    ,		dbo.t015_mm_contactos.f015_id_pais																		AS country_code
                                    ,		'False'																									AS use_incentives

                                   -- ,		CASE dbo.T461_CM_DOCTO_FACTURA_VENTA.F461_ID_CLASE_DOCTO WHEN 521 THEN (CONVERT(float, dbo.T470_CM_MOVTO_INVENT.F470_VLR_BRUTO) * (- 1)) 
		                           --        WHEN 525 THEN (CONVERT(float, dbo.T470_CM_MOVTO_INVENT.F470_VLR_BRUTO) * (- 1)) ELSE (CONVERT(float, dbo.T470_CM_MOVTO_INVENT.F470_VLR_BRUTO)) END	AS total_value
				                    ,		CONVERT(float, dbo.T461_CM_DOCTO_FACTURA_VENTA.f461_vlr_neto) AS total_value

                                    ,		CONVERT(float, dbo.T461_CM_DOCTO_FACTURA_VENTA.f461_vlr_neto)											AS end_value

                                    ,		dbo.t015_mm_contactos.f015_id_depto +  dbo.t015_mm_contactos.f015_id_ciudad								AS delivery_city_code

                                    ,		dbo.t015_mm_contactos.f015_id_pais																		AS delivery_country_code

                                    ,		Case when datediff(day,f461_id_fecha,f353_fecha_vcto) between 0 and 1  THEN '1' 
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) between 2 and 3  Then '3'
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) between 4 and 5  Then '5'
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) = 6    Then '6'
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) = 7    Then '7'
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) between 8 and 9   Then '28' 
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) between 10 and 12 THEN '38'
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) between 13 and 15 Then '106'
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) between 16 and 20 THEN '105'
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) between 21 and 25 THEN '171'
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) between 26 and 31 THEN '75'
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) between 32 and 35 THEN '255'								 
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) between 36 and 40 Then '40'
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) between 41 and 45 THEN '109'
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) = 46    Then '147'
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) between 47 and 50 THEN '271'
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) = 51    Then '160'
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) between 52 and 54 THEN '54'
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) between 55 and 60 THEN '101'
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) = 61    Then '175'
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) between 62 and 90 THEN '90'								  
								                 when datediff(day,f461_id_fecha,f353_fecha_vcto) between 91 and 120 Then '190'
							                     when datediff(day,f461_id_fecha,f353_fecha_vcto) between 121 and 160 Then '167'
		                                    else '1' End AS payment_method

                                   -- ,		CP.f208_descripcion	AS CodigoPago2--**
                                    ,		'C'												AS delivery_warehouse  --C COMPRADOR | V VENDEDOR
                                    --,		dbo.v121.v121_referencia						AS product 

                                   -- ,		CASE dbo.T461_CM_DOCTO_FACTURA_VENTA.F461_ID_CLASE_DOCTO WHEN 521 THEN (CONVERT(float, dbo.T470_CM_MOVTO_INVENT.F470_CANT_1) * (- 1)) 
		                           --         WHEN 525 THEN (CONVERT(float, dbo.T470_CM_MOVTO_INVENT.F470_CANT_1) * (- 1)) ELSE (CONVERT(float, dbo.T470_CM_MOVTO_INVENT.F470_CANT_1)) END  AS quantity

                                    --,		CONVERT(float, dbo.t470_cm_movto_invent.f470_precio_uni)												AS unitary_price
                                    --,		CONVERT(float, dbo.t470_cm_movto_invent.f470_vlr_imp/dbo.t470_cm_movto_invent.f470_vlr_bruto, 2) 		AS tax
                                   -- ,		CONVERT(float, dbo.t470_cm_movto_invent.f470_vlr_neto)													AS total_price
				                   --select *
                                    FROM    dbo.v121 INNER JOIN
                                            dbo.t470_cm_movto_invent ON dbo.v121.v121_rowid_item_ext = dbo.t470_cm_movto_invent.f470_rowid_item_ext INNER JOIN
                                            dbo.t461_cm_docto_factura_venta ON dbo.t470_cm_movto_invent.f470_rowid_docto_fact = dbo.t461_cm_docto_factura_venta.f461_rowid_docto INNER JOIN
                                            dbo.t200_mm_terceros ON dbo.t200_mm_terceros.f200_rowid = dbo.t461_cm_docto_factura_venta.f461_rowid_tercero_fact INNER JOIN
                                            dbo.t350_co_docto_contable ON dbo.t461_cm_docto_factura_venta.f461_rowid_docto = dbo.t350_co_docto_contable.f350_rowid INNER JOIN
                                            dbo.t201_mm_clientes ON dbo.t201_mm_clientes.f201_rowid_tercero = dbo.t200_mm_terceros.f200_rowid AND dbo.t201_mm_clientes.f201_id_sucursal = dbo.t461_cm_docto_factura_venta.f461_id_sucursal_fact INNER JOIN
                                            dbo.t010_mm_companias ON dbo.t350_co_docto_contable.f350_id_cia = dbo.t010_mm_companias.f010_id INNER JOIN
                                            dbo.t353_co_saldo_abierto ON dbo.t350_co_docto_contable.f350_rowid = dbo.t353_co_saldo_abierto.f353_rowid_docto LEFT OUTER JOIN
                                            dbo.t208_mm_condiciones_pago CP ON dbo.t201_mm_clientes.f201_id_cia = CP.f208_id_cia AND 
                                            dbo.t201_mm_clientes.f201_id_cond_pago = CP.f208_id LEFT OUTER JOIN
                                            dbo.t015_mm_contactos ON dbo.t201_mm_clientes.f201_rowid_contacto = dbo.t015_mm_contactos.f015_rowid

                                    WHERE   v121_id_cia = 1
		                            and     dbo.t350_co_docto_contable.f350_ind_estado = 1
		                            and     f470_id_concepto = 501         
                                    --AND		CONVERT(nvarchar(10), dbo.t461_cm_docto_factura_venta.f461_id_fecha, 23) BETWEEN convert(date,getdate()-1,112) and convert(date,getdate()-1,112)
					
                and v121_id_tipo_inv_serv in (
                'AV001',   
                'AV002',    
                'CS41056030',
                'PR001',    
                 'PT001',    
                'PT002',    
                'PT003',    
                'PT004',    
                'PT005',    
                'PT006',    
                'PT007',    
                'PT008',    
                'PT009',    
                'PT010',    
                'PT012'    

                )
                and f200_id not in (
                select '333333333' f200_id
                union all
                select '77777777' f200_id
                union all
                select '88888888' f200_id
                union all
                 select '99999999' f200_id
                 union all
                select distinct  f200_id from t200_mm_terceros
                where f200_id_cia = 1 and
                f200_id_tipo_ident not in ('N') and
                f200_id not in (
                '788440',
                '2401484',
                '2514307',
                '6500297',
                '11304997',
                '12979267',
                '12981811',
                '13250502',
                '14212809',
                '16265068',
                '16354374',
                '16734344',
                '16736580',
                '21778027',
                '21778718',
                '29363585',
                '29886815',
                '29886815',
                '30708594',
                '30732216',
                '31575077',
                '35893830',
                '59663114',
                '70558419',
                '70692652',
                '70828170',
                '71577184',
                '79242930',
                '91259419',
                '93387009',
                '900977226')
                 )
               and f350_id_tipo_docto = 'S87'
               and f350_consec_docto in (6015)
                group by f350_id_co, f350_id_tipo_docto, f350_consec_docto, f461_vlr_neto, f461_id_fecha, f353_fecha_vcto,f350_id_moneda_docto, f010_nit,f200_id_tipo_ident,f200_nit,f200_id,f200_dv_nit
                ,f200_razon_social ,f200_nombres ,f200_apellido1,f200_nombre_est,f015_telefono,f015_email,f015_id_depto,f015_id_ciudad,f015_id_pais
                having 	sum(f470_vlr_neto)>=3340000
                 order by 2 asc", SqlConnection)
        cmd.CommandType = CommandType.Text
        cmd.CommandTimeout = 90000000
        da.SelectCommand = cmd


        Try
            da.Fill(ds, "Ventas")

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            SqlConnection.Close()
        End Try

        Return ds

    End Function

    Public Function ConsultarProductosFacturas(ByVal tipo As String, ByVal factura As String) As DataSet

        Dim SqlConnection As New SqlConnection(My.Settings.strConexionGT)

        SqlConnection.Open()

        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand("SELECT    

                    --f350_id_tipo_docto
					--,f350_consec_docto
                    		dbo.v121.v121_referencia						AS product 

                    ,convert(decimal(10,2),(CONVERT(float, dbo.T470_CM_MOVTO_INVENT.F470_CANT_1))*CONVERT(float,f122_volumen))  AS quantity

                    ,Round(((CONVERT(float, dbo.t470_cm_movto_invent.f470_vlr_bruto)-CONVERT(float, dbo.t470_cm_movto_invent.f470_vlr_dscto_linea))/ f470_cant_1) /CONVERT(float,f122_volumen),5,0) unitary_price
                    ,		replace(CONVERT(float, dbo.t470_cm_movto_invent.f470_vlr_imp/dbo.t470_cm_movto_invent.f470_vlr_bruto, 2),',','.') 		AS tax
                    ,		CONVERT(float, dbo.t470_cm_movto_invent.f470_vlr_neto)													AS total_price

                    FROM    dbo.v121 INNER JOIN
                            (SELECT distinct f122_id_cia,f122_rowid_item, f122_volumen
							FROM t122_mc_items_unidades where f122_volumen > 0) factor on factor.f122_id_cia = v121_id_cia and f122_rowid_item = v121_rowid_item  INNER JOIN
                            dbo.t470_cm_movto_invent ON dbo.v121.v121_rowid_item_ext = dbo.t470_cm_movto_invent.f470_rowid_item_ext INNER JOIN
                            dbo.t461_cm_docto_factura_venta ON dbo.t470_cm_movto_invent.f470_rowid_docto_fact = dbo.t461_cm_docto_factura_venta.f461_rowid_docto INNER JOIN
                            dbo.t200_mm_terceros ON dbo.t200_mm_terceros.f200_rowid = dbo.t461_cm_docto_factura_venta.f461_rowid_tercero_fact INNER JOIN
                            dbo.t350_co_docto_contable ON dbo.t461_cm_docto_factura_venta.f461_rowid_docto = dbo.t350_co_docto_contable.f350_rowid 

                    WHERE   v121_id_cia = 1
		            and     dbo.t350_co_docto_contable.f350_ind_estado = 1
		            and     f470_id_concepto = 501         
                    --AND		CONVERT(nvarchar(10), dbo.t461_cm_docto_factura_venta.f461_id_fecha, 23) BETWEEN '2021-01-01' and '2021-01-10'
					AND		f461_vlr_bruto >= 3340000 --se sugiere que sea un dato que se tome del ERP
and v121_id_tipo_inv_serv in (
'AV001',   
'AV002',    
'CS41056030',
'PR001',    
 'PT001',    
'PT002',    
'PT003',    
'PT004',    
'PT005',    
'PT006',    
'PT007',    
'PT008',    
'PT009',    
'PT010',    
'PT012'    

)
and f200_id not in (
select '333333333' f200_id
union all
select '77777777' f200_id
union all
select '88888888' f200_id
union all
 select '99999999' f200_id
 union all
select distinct  f200_id from t200_mm_terceros
where f200_id_cia = 1 and
f200_id_tipo_ident not in ('N') and
f200_id not in (
'788440',
'2401484',
'2514307',
'6500297',
'11304997',
'12979267',
'12981811',
'13250502',
'14212809',
'16265068',
'16354374',
'16734344',
'16736580',
'21778027',
'21778718',
'29363585',
'29886815',
'29886815',
'30708594',
'30732216',
'31575077',
'35893830',
'59663114',
'70558419',
'70692652',
'70828170',
'71577184',
'79242930',
'91259419',
'93387009',
'900977226')
 )
                    and     f350_id_tipo_docto = '" + tipo + "'
					and     f350_consec_docto = " + factura, SqlConnection)
        cmd.CommandType = CommandType.Text
        da.SelectCommand = cmd

        Try
            da.Fill(ds, "Productos")

        Catch ex As Exception
            'MsgBox(ex.Message)
        Finally
            SqlConnection.Close()
        End Try

        Return ds

    End Function


End Class
