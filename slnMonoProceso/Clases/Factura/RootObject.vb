﻿Public Class PeriodoFacturacion
    Public Property fechaInicio As String
    Public Property horaInicio As String
    Public Property fechaFin As String
    Public Property horaFin As String
End Class

Public Class TasaCambio
    Public Property fechaCambio As String
    Public Property codigoMonedaFacturado As String
    Public Property codigoMonedaCambio As String
    Public Property baseCambioFacturado As String
    Public Property baseCambio As String
    Public Property trm As String
End Class

Public Class Pago
    Public Property id As String
    Public Property codigoMedioPago As String
    Public Property fechaVencimiento As String
    Public Property listaIdentificadoresPago As List(Of String)
End Class

Public Class DocumentosAnexo
    Public Property id As String
    Public Property tipo As String
    Public Property fechaEmision As String
End Class

Public Class ListaImpuesto
    Public Property codigo As String
    Public Property nombre As String
    Public Property baseGravable As String
    Public Property porcentaje As String
    Public Property valor As String
    Public Property codigoUnidad As String
    Public Property unidad As String
    Public Property valorPorUnidad As String
End Class

Public Class listaDeducciones
    Public Property codigo As String
    Public Property nombre As String
    Public Property baseGravable As String
    Public Property porcentaje As String
    Public Property valor As String
End Class

Public Class GruposImpuesto
    Public Property codigo As String
    Public Property total As String
    Public Property listaImpuestos As List(Of ListaImpuesto)
End Class
Public Class gruposDeducciones
    Public Property codigo As String
    Public Property total As String
    Public Property listaDeducciones As List(Of listaDeducciones)
End Class

Public Class gruposDescuentos
    Public Property codigo As String
    Public Property total As String
    Public Property lsitaDescuentos As List(Of ListaCargosDescuentos)
End Class

Public Class Contacto
    Public Property nombre As String
    Public Property telefono As String
    Public Property fax As String
    Public Property email As String
    Public Property observaciones As String
End Class

Public Class Direccion
    Public Property codigoPais As String
    Public Property nombrePais As String
    Public Property codigoLenguajePais As String
    Public Property codigoDepartamento As String
    Public Property nombreDepartamento As String
    Public Property codigoCiudad As String
    Public Property nombreCiudad As String
    Public Property direccionFisica As String
    Public Property codigoPostal As String
End Class

Public Class DireccionFiscal
    Public Property codigoPais As String
    Public Property nombrePais As String
    Public Property codigoLenguajePais As String
    Public Property codigoDepartamento As String
    Public Property nombreDepartamento As String
    Public Property codigoCiudad As String
    Public Property nombreCiudad As String
    Public Property direccionFisica As String
    Public Property codigoPostal As String
End Class

Public Class ListaResponsabilidadesTributaria
    Public Property codigo As String
    Public Property nombre As String
    Public Property descripcion As String
End Class

Public Class Sucursal
    Public Property id As String
    Public Property numeroMatricula As String
    Public Property razonSocial As String
    Public Property prefijoFacturacion As String
    Public Property direccion As String
    Public Property telefono As String
    Public Property email As String
End Class

Public Class Facturador
    Public Property razonSocial As String
    Public Property nombreRegistrado As String
    Public Property tipoIdentificacion As String
    Public Property identificacion As String
    Public Property digitoVerificacion As String
    Public Property naturaleza As String
    Public Property codigoRegimen As String
    Public Property responsabilidadFiscal As String
    Public Property codigoImpuesto As String
    Public Property nombreImpuesto As String
    Public Property telefono As String
    Public Property email As String
    Public Property contacto As Contacto
    Public Property direccion As Direccion
    Public Property direccionFiscal As DireccionFiscal
    Public Property listaResponsabilidadesTributarias As List(Of ListaResponsabilidadesTributaria)
    Public Property codigoCIIU As String
    Public Property sucursal As Sucursal
End Class

Public Class Contacto2
    Public Property nombre As String
    Public Property telefono As String
    Public Property fax As String
    Public Property email As String
    Public Property observaciones As String
End Class

Public Class Direccion2
    Public Property codigoPais As String
    Public Property nombrePais As String
    Public Property codigoLenguajePais As String
    Public Property codigoDepartamento As String
    Public Property nombreDepartamento As String
    Public Property codigoCiudad As String
    Public Property nombreCiudad As String
    Public Property direccionFisica As String
    Public Property codigoPostal As String
End Class

Public Class DireccionFiscal2
    Public Property codigoPais As String
    Public Property nombrePais As String
    Public Property codigoLenguajePais As String
    Public Property codigoDepartamento As String
    Public Property nombreDepartamento As String
    Public Property codigoCiudad As String
    Public Property nombreCiudad As String
    Public Property direccionFisica As String
    Public Property codigoPostal As String
End Class

Public Class ListaResponsabilidadesTributaria2
    Public Property codigo As String
    Public Property nombre As String
    Public Property descripcion As String
End Class

Public Class Sucursal2
    Public Property id As String
    Public Property numeroMatricula As String
    Public Property razonSocial As String
    Public Property prefijoFacturacion As String
    Public Property direccion As String
    Public Property telefono As String
    Public Property email As String
End Class

Public Class CentroCosto
    Public Property id As String
    Public Property nombre As String
    Public Property direccion As String
    Public Property telefono As String
    Public Property email As String
End Class

Public Class Adquiriente
    Public Property razonSocial As String
    Public Property nombreRegistrado As String
    Public Property tipoIdentificacion As String
    Public Property identificacion As String
    Public Property digitoVerificacion As String
    Public Property naturaleza As String
    Public Property codigoRegimen As String
    Public Property responsabilidadFiscal As String
    Public Property codigoImpuesto As String
    Public Property nombreImpuesto As String
    Public Property telefono As String
    Public Property email As String
    Public Property contacto As Contacto2
    Public Property direccion As Direccion2
    Public Property direccionFiscal As DireccionFiscal2
    Public Property listaResponsabilidadesTributarias As List(Of ListaResponsabilidadesTributaria2)
    Public Property codigoCIIU As String
    Public Property sucursal As Sucursal2
    Public Property centroCosto As CentroCosto
End Class

Public Class Autorizado
    Public Property razonSocial As String
    Public Property tipoIdentificacion As String
    Public Property identificacion As String
    Public Property digitoVerificacion As String
End Class

Public Class Contacto3
    Public Property nombre As String
    Public Property telefono As String
    Public Property fax As String
    Public Property email As String
    Public Property observaciones As String
End Class

Public Class Direccion3
    Public Property codigoPais As String
    Public Property nombrePais As String
    Public Property codigoLenguajePais As String
    Public Property codigoDepartamento As String
    Public Property nombreDepartamento As String
    Public Property codigoCiudad As String
    Public Property nombreCiudad As String
    Public Property direccionFisica As String
    Public Property codigoPostal As String
End Class

Public Class DireccionFiscal3
    Public Property codigoPais As String
    Public Property nombrePais As String
    Public Property codigoLenguajePais As String
    Public Property codigoDepartamento As String
    Public Property nombreDepartamento As String
    Public Property codigoCiudad As String
    Public Property nombreCiudad As String
    Public Property direccionFisica As String
    Public Property codigoPostal As String
End Class

Public Class ListaResponsabilidadesTributaria3
    Public Property codigo As String
    Public Property nombre As String
    Public Property descripcion As String
End Class

Public Class Transportista
    Public Property razonSocial As String
    Public Property nombreRegistrado As String
    Public Property tipoIdentificacion As String
    Public Property identificacion As String
    Public Property digitoVerificacion As String
    Public Property naturaleza As String
    Public Property codigoRegimen As String
    Public Property responsabilidadFiscal As String
    Public Property codigoImpuesto As String
    Public Property nombreImpuesto As String
    Public Property telefono As String
    Public Property email As String
    Public Property contacto As Contacto3
    Public Property direccion As Direccion3
    Public Property direccionFiscal As DireccionFiscal3
    Public Property listaResponsabilidadesTributarias As List(Of ListaResponsabilidadesTributaria3)
    Public Property numeroMatricula As String
End Class

Public Class DireccionEntrega
    Public Property codigoPais As String
    Public Property nombrePais As String
    Public Property codigoLenguajePais As String
    Public Property codigoDepartamento As String
    Public Property nombreDepartamento As String
    Public Property codigoCiudad As String
    Public Property nombreCiudad As String
    Public Property direccionFisica As String
    Public Property codigoPostal As String
End Class

Public Class CondicionesEntrega
    Public Property id As String
    Public Property incoterm As String
    Public Property metodoPago As String
    Public Property riesgoPerdida As String
End Class

Public Class Entrega
    Public Property fechaSalidaProductos As String
    Public Property horaSalidaProductos As String
    Public Property tieneTransportista As Boolean
    Public Property transportista As Transportista
    Public Property direccionEntrega As DireccionEntrega
    Public Property condicionesEntrega As CondicionesEntrega
End Class

Public Class ListaCaracteristica
    Public Property codigo As String
    Public Property valor As String
End Class

Public Class Mandatorio
    Public Property tipoIdentificacion As String
    Public Property identificacion As String
    Public Property digitoVerificacion As String
End Class

Public Class Item
    Public Property marca As String
    Public Property modelo As String
    Public Property codigoArticuloVendedor As String
    Public Property codigoExtendidoVendedor As String
    Public Property codigoEstandar As String
    Public Property nombreEstandar As String
    Public Property descripcion As String
    Public Property cantidadPaquete As String
    Public Property listaCaracteristicas As List(Of ListaCaracteristica)
    Public Property mandatorio As Mandatorio
End Class

Public Class ListaImpuesto2
    Public Property codigo As String
    Public Property nombre As String
    Public Property baseGravable As String
    Public Property porcentaje As String
    Public Property valor As String
    Public Property codigoUnidad As String
    Public Property unidad As String
    Public Property valorPorUnidad As String
End Class

Public Class ListaProducto
    Public Property numeroLinea As String
    Public Property informacion As String
    Public Property cantidad As String
    Public Property valorTotal As String
    Public Property idProducto As String
    Public Property codigoPrecio As String
    Public Property valorUnitario As String
    Public Property cantidadReal As String
    Public Property codigoUnidad As String
    Public Property esMuestraComercial As Boolean
    Public Property item As Item
    Public Property listaImpuestos As List(Of ListaImpuesto2)
    Public Property listaCargosDescuentos As List(Of ListaCargosDescuentos)
    'Public Property listaDeducciones As List(Of listaDeducciones)
End Class

Public Class Numeracion
    Public Property prefijo As String
    Public Property desde As String
    Public Property hasta As String
    Public Property fechaInicio As String
    Public Property fechaFin As String
End Class
Public Class Resolucion
    Public Property numero As String
    Public Property fechaInicio As String
    Public Property fechaFin As String
    Public Property numeracion As Numeracion
End Class
Public Class ListaCargosDescuentos
    Public Property id As String
    Public Property esCargo As Boolean
    Public Property codigo As String
    Public Property razon As String
    Public Property base As String
    Public Property porcentaje As String
    Public Property valor As String
End Class
Public Class listaDocumentosReferenciados
    Public Property id As String
    Public Property tipo As String
    Public Property fecha As String
    Public Property algoritmo As String
    Public Property cufe As String
End Class
Public Class RootObject
    Public Property tipoDocumento As String
    Public Property versionDocumento As String
    Public Property registrar As Boolean
    Public Property control As String
    Public Property cvcc As String
    Public Property formato As String
    Public Property codigoTipoDocumento As String
    Public Property tipoOperacion As String
    Public Property prefijoDocumento As String
    Public Property numeroDocumento As String
    Public Property fechaEmision As String
    Public Property horaEmision As String
    Public Property periodoFacturacion As PeriodoFacturacion
    Public Property numeroLineas As String
    Public Property subtotal As String
    Public Property totalBaseImponible As String
    Public Property subtotalMasTributos As String
    Public Property totalDescuentos As String
    Public Property totalCargos As String
    Public Property totalAnticipos As String
    Public Property total As String
    Public Property codigoMoneda As String
    Public Property tasaCambio As TasaCambio
    Public Property pago As Pago
    Public Property listaDescripciones As List(Of String)
    Public Property listaDocumentosReferenciados As List(Of listaDocumentosReferenciados)
    Public Property documentosAnexos As List(Of DocumentosAnexo)
    Public Property gruposImpuestos As List(Of GruposImpuesto)
    Public Property gruposDeducciones As List(Of gruposDeducciones)
    Public Property facturador As Facturador
    Public Property adquiriente As Adquiriente
    Public Property autorizado As Autorizado
    Public Property entrega As Entrega
    Public Property urlAnexos As String
    Public Property base64 As String
    Public Property posicionXCufe As String
    Public Property posicionYCufe As String
    Public Property rotacionCufe As String
    Public Property fuenteCufe As String
    Public Property posicionXQr As String
    Public Property posicionYQr As String
    Public Property listaProductos As List(Of ListaProducto)
    Public Property resolucion As Resolucion

End Class
