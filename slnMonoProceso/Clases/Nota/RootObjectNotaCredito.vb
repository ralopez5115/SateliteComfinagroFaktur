﻿Public Class PeriodoFacturacionNC
    Public Property fechaInicio As String
    Public Property horaInicio As String
    Public Property fechaFin As String
    Public Property horaFin As String
End Class

Public Class TasaCambioNC
    Public Property fechaCambio As String
    Public Property codigoMonedaFacturado As String
    Public Property codigoMonedaCambio As String
    Public Property baseCambioFacturado As String
    Public Property baseCambio As String
    Public Property trm As String
End Class

Public Class PagoNC
    Public Property id As String
    Public Property codigoMedioPago As String
    Public Property fechaVencimiento As String
    Public Property listaIdentificadoresPago As List(Of String)
End Class

Public Class ListaDocumentosReferenciadoNC
    Public Property id As String
    Public Property tipo As String
    Public Property fecha As String
    Public Property algoritmo As String
    Public Property cufe As String
End Class

Public Class DocumentosAnexoNC
    Public Property id As String
    Public Property tipo As String
    Public Property fechaEmision As String
End Class

Public Class ListaImpuestoNC
    Public Property codigo As String
    Public Property nombre As String
    Public Property baseGravable As String
    Public Property porcentaje As String
    Public Property valor As String
    Public Property codigoUnidad As String
    Public Property unidad As String
    Public Property valorPorUnidad As String
End Class

Public Class GruposImpuestoNC
    Public Property codigo As String
    Public Property total As String
    Public Property listaImpuestos As List(Of ListaImpuestoNC)
End Class

Public Class ContactoNC
    Public Property nombre As String
    Public Property telefono As String
    Public Property fax As String
    Public Property email As String
    Public Property observaciones As String
End Class

Public Class DireccionNC
    Public Property codigoPais As String
    Public Property nombrePais As String
    Public Property codigoLenguajePais As String
    Public Property codigoDepartamento As String
    Public Property nombreDepartamento As String
    Public Property codigoCiudad As String
    Public Property nombreCiudad As String
    Public Property direccionFisica As String
    Public Property codigoPostal As String
End Class

Public Class DireccionFiscalNC
    Public Property codigoPais As String
    Public Property nombrePais As String
    Public Property codigoLenguajePais As String
    Public Property codigoDepartamento As String
    Public Property nombreDepartamento As String
    Public Property codigoCiudad As String
    Public Property nombreCiudad As String
    Public Property direccionFisica As String
    Public Property codigoPostal As String
End Class

Public Class ListaResponsabilidadesTributariaNC
    Public Property codigo As String
    Public Property nombre As String
    Public Property descripcion As String
End Class

Public Class SucursalNC
    Public Property id As String
    Public Property numeroMatricula As String
    Public Property razonSocial As String
    Public Property prefijoFacturacion As String
    Public Property direccion As String
    Public Property telefono As String
    Public Property email As String
End Class

Public Class FacturadorNC
    Public Property razonSocial As String
    Public Property nombreRegistrado As String
    Public Property tipoIdentificacion As String
    Public Property identificacion As String
    Public Property digitoVerificacion As String
    Public Property naturaleza As String
    Public Property codigoRegimen As String
    Public Property responsabilidadFiscal As String
    Public Property codigoImpuesto As String
    Public Property nombreImpuesto As String
    Public Property telefono As String
    Public Property email As String
    Public Property contacto As ContactoNC
    Public Property direccion As DireccionNC
    Public Property direccionFiscal As DireccionFiscalNC
    Public Property listaResponsabilidadesTributarias As List(Of ListaResponsabilidadesTributariaNC)
    Public Property codigoCIIU As String
    Public Property sucursal As SucursalNC
End Class

Public Class Contacto2NC
    Public Property nombre As String
    Public Property telefono As String
    Public Property fax As String
    Public Property email As String
    Public Property observaciones As String
End Class

Public Class Direccion2NC
    Public Property codigoPais As String
    Public Property nombrePais As String
    Public Property codigoLenguajePais As String
    Public Property codigoDepartamento As String
    Public Property nombreDepartamento As String
    Public Property codigoCiudad As String
    Public Property nombreCiudad As String
    Public Property direccionFisica As String
    Public Property codigoPostal As String
End Class

Public Class DireccionFiscal2NC
    Public Property codigoPais As String
    Public Property nombrePais As String
    Public Property codigoLenguajePais As String
    Public Property codigoDepartamento As String
    Public Property nombreDepartamento As String
    Public Property codigoCiudad As String
    Public Property nombreCiudad As String
    Public Property direccionFisica As String
    Public Property codigoPostal As String
End Class

Public Class ListaResponsabilidadesTributaria2NC
    Public Property codigo As String
    Public Property nombre As String
    Public Property descripcion As String
End Class

Public Class Sucursal2NC
    Public Property id As String
    Public Property numeroMatricula As String
    Public Property razonSocial As String
    Public Property prefijoFacturacion As String
    Public Property direccion As String
    Public Property telefono As String
    Public Property email As String
End Class

Public Class CentroCostoNC
    Public Property id As String
    Public Property nombre As String
    Public Property direccion As String
    Public Property telefono As String
    Public Property email As String
End Class

Public Class AdquirienteNC
    Public Property razonSocial As String
    Public Property nombreRegistrado As String
    Public Property tipoIdentificacion As String
    Public Property identificacion As String
    Public Property digitoVerificacion As String
    Public Property naturaleza As String
    Public Property codigoRegimen As String
    Public Property responsabilidadFiscal As String
    Public Property codigoImpuesto As String
    Public Property nombreImpuesto As String
    Public Property telefono As String
    Public Property email As String
    Public Property contacto As Contacto2NC
    Public Property direccion As Direccion2NC
    Public Property direccionFiscal As DireccionFiscal2NC
    Public Property listaResponsabilidadesTributarias As List(Of ListaResponsabilidadesTributaria2NC)
    Public Property codigoCIIU As String
    Public Property sucursal As Sucursal2NC
    Public Property centroCosto As CentroCostoNC
End Class

Public Class AutorizadoNC
    Public Property razonSocial As String
    Public Property tipoIdentificacion As String
    Public Property identificacion As String
    Public Property digitoVerificacion As String
End Class

Public Class Contacto3NC
    Public Property nombre As String
    Public Property telefono As String
    Public Property fax As String
    Public Property email As String
    Public Property observaciones As String
End Class

Public Class Direccion3NC
    Public Property codigoPais As String
    Public Property nombrePais As String
    Public Property codigoLenguajePais As String
    Public Property codigoDepartamento As String
    Public Property nombreDepartamento As String
    Public Property codigoCiudad As String
    Public Property nombreCiudad As String
    Public Property direccionFisica As String
    Public Property codigoPostal As String
End Class

Public Class DireccionFiscal3NC
    Public Property codigoPais As String
    Public Property nombrePais As String
    Public Property codigoLenguajePais As String
    Public Property codigoDepartamento As String
    Public Property nombreDepartamento As String
    Public Property codigoCiudad As String
    Public Property nombreCiudad As String
    Public Property direccionFisica As String
    Public Property codigoPostal As String
End Class

Public Class ListaResponsabilidadesTributaria3NC
    Public Property codigo As String
    Public Property nombre As String
    Public Property descripcion As String
End Class

Public Class TransportistaNC
    Public Property razonSocial As String
    Public Property nombreRegistrado As String
    Public Property tipoIdentificacion As String
    Public Property identificacion As String
    Public Property digitoVerificacion As String
    Public Property naturaleza As String
    Public Property codigoRegimen As String
    Public Property responsabilidadFiscal As String
    Public Property codigoImpuesto As String
    Public Property nombreImpuesto As String
    Public Property telefono As String
    Public Property email As String
    Public Property contacto As Contacto3NC
    Public Property direccion As Direccion3NC
    Public Property direccionFiscal As DireccionFiscal3NC
    Public Property listaResponsabilidadesTributarias As List(Of ListaResponsabilidadesTributaria3NC)
    Public Property numeroMatricula As String
End Class

Public Class DireccionEntregaNC
    Public Property codigoPais As String
    Public Property nombrePais As String
    Public Property codigoLenguajePais As String
    Public Property codigoDepartamento As String
    Public Property nombreDepartamento As String
    Public Property codigoCiudad As String
    Public Property nombreCiudad As String
    Public Property direccionFisica As String
    Public Property codigoPostal As String
End Class

Public Class EntregaNC
    Public Property fechaSalidaProductos As String
    Public Property horaSalidaProductos As String
    Public Property tieneTransportista As Boolean
    Public Property transportista As TransportistaNC
    Public Property direccionEntrega As DireccionEntregaNC
End Class

Public Class ListaCaracteristicaNC
    Public Property codigo As String
    Public Property valor As String
End Class

Public Class MandatorioNC
    Public Property tipoIdentificacion As String
    Public Property identificacion As String
    Public Property digitoVerificacion As String
End Class

Public Class ItemNC
    Public Property marca As String
    Public Property modelo As String
    Public Property codigoArticuloVendedor As String
    Public Property codigoExtendidoVendedor As String
    Public Property codigoEstandar As String
    Public Property nombreEstandar As String
    Public Property descripcion As String
    Public Property cantidadPaquete As String
    Public Property listaCaracteristicas As List(Of ListaCaracteristicaNC)
    Public Property mandatorio As MandatorioNC
End Class

Public Class ListaImpuesto2NC
    Public Property codigo As String
    Public Property nombre As String
    Public Property baseGravable As String
    Public Property porcentaje As String
    Public Property valor As String
    Public Property codigoUnidad As String
    Public Property unidad As String
    Public Property valorPorUnidad As String
End Class

Public Class ListaProductoNC
    Public Property numeroLinea As String
    Public Property informacion As String
    Public Property cantidad As String
    Public Property valorTotal As String
    Public Property idProducto As String
    Public Property codigoPrecio As String
    Public Property valorUnitario As String
    Public Property cantidadReal As String
    Public Property codigoUnidad As String
    Public Property esMuestraComercial As Boolean
    Public Property item As ItemNC
    Public Property listaImpuestos As List(Of ListaImpuesto2NC)
    Public Property listaCargosDescuentos As List(Of ListaCargosDescuentos2)
End Class

Public Class ListaCorreccioneNC
    Public Property id As String
    Public Property codigo As String
    Public Property descripcion As String
End Class
Public Class ListaCargosDescuentos2
    Public Property id As String
    Public Property esCargo As Boolean
    Public Property codigo As String
    Public Property razon As String
    Public Property base As String
    Public Property porcentaje As String
    Public Property valor As String
End Class
Public Class RootObjectNotaCredito
    Public Property tipoDocumento As String
    Public Property versionDocumento As String
    Public Property registrar As Boolean
    Public Property control As String
    Public Property cvcc As String
    Public Property formato As String
    Public Property codigoTipoDocumento As String
    Public Property tipoOperacion As String
    Public Property prefijoDocumento As String
    Public Property numeroDocumento As String
    Public Property fechaEmision As String
    Public Property horaEmision As String
    Public Property periodoFacturacion As PeriodoFacturacionNC
    Public Property numeroLineas As String
    Public Property subtotal As String
    Public Property totalBaseImponible As String
    Public Property subtotalMasTributos As String
    Public Property totalDescuentos As String
    Public Property totalCargos As String
    Public Property totalAnticipos As String
    Public Property redondeo As String
    Public Property total As String
    Public Property codigoMoneda As String
    Public Property tasaCambio As TasaCambioNC
    Public Property pago As PagoNC
    Public Property listaDescripciones As List(Of String)
    Public Property listaDocumentosReferenciados As List(Of ListaDocumentosReferenciadoNC)
    Public Property documentosAnexos As List(Of DocumentosAnexoNC)
    Public Property gruposImpuestos As List(Of GruposImpuestoNC)
    Public Property facturador As FacturadorNC
    Public Property adquiriente As AdquirienteNC
    Public Property autorizado As AutorizadoNC
    Public Property entrega As EntregaNC
    Public Property fechaHoraRecepcion As Object
    Public Property cufe As String
    Public Property qr As String
    Public Property urlAnexos As String
    Public Property base64 As String
    Public Property posicionXCufe As String
    Public Property posicionYCufe As String
    Public Property rotacionCufe As String
    Public Property fuenteCufe As String
    Public Property posicionXQr As String
    Public Property posicionYQr As String
    Public Property listaProductos As List(Of ListaProductoNC)
    Public Property listaCorrecciones As List(Of ListaCorreccioneNC)
End Class
