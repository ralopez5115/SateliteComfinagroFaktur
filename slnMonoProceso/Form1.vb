﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports System.Security.Cryptography.X509Certificates
Imports System.ServiceModel
Imports System.Text
Imports System.Xml
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Net.Security

Public Class Form1

    Dim objDatos As New clsOperacionesSQL
    'Dim objCorreo As New clsCorreo
    'Dim objTarea As New clsTarea

    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load



        Try
            ArmarFactura()
        Catch ex As Exception
            'MsgBox("Error" + ex.Message)
            Dim clTarea As New clsTarea

        End Try
        'MessageBox.Show("fin")
        Close()
    End Sub

    Public Function ArmarFactura() As FacturaFaktur
        Dim Factura As New FacturaFaktur
        Dim datos = objDatos.ConsultarFacturas()

        'MsgBox("ya consulto" + datos.Tables.Count.ToString)

        If datos.Tables.Count > 0 Then
            If datos.Tables(0).Rows.Count > 0 Then

                For Index = 1 To datos.Tables(0).Rows.Count
                    Factura.operation_id = datos.Tables(0).Rows(Index - 1).Item("operation_id").ToString
                    Factura.operation_type = datos.Tables(0).Rows(Index - 1).Item("operation_type").ToString
                    Factura.operation_issue_date = datos.Tables(0).Rows(Index - 1).Item("operation_issue_date").ToString
                    Factura.operation_filing_date = datos.Tables(0).Rows(Index - 1).Item("operation_filing_date").ToString
                    Factura.operation_posting_date = datos.Tables(0).Rows(Index - 1).Item("operation_posting_date").ToString
                    Factura.operation_expiration_date = datos.Tables(0).Rows(Index - 1).Item("operation_expiration_date").ToString
                    Factura.currency = datos.Tables(0).Rows(Index - 1).Item("currency").ToString
                    Factura.main_sender = datos.Tables(0).Rows(Index - 1).Item("main_sender").ToString

                    Factura.secondary_sender = New secondary_sender

                    Factura.secondary_sender.document_type = datos.Tables(0).Rows(Index - 1).Item("document_type").ToString
                    Factura.secondary_sender.document_number = datos.Tables(0).Rows(Index - 1).Item("document_number").ToString
                    Factura.secondary_sender.verification_digit = datos.Tables(0).Rows(Index - 1).Item("verification_digit").ToString
                    Factura.secondary_sender.person_type = datos.Tables(0).Rows(Index - 1).Item("person_type").ToString
                    Factura.secondary_sender.social_reason = datos.Tables(0).Rows(Index - 1).Item("social_reason").ToString
                    Factura.secondary_sender.first_name = datos.Tables(0).Rows(Index - 1).Item("first_name").ToString
                    Factura.secondary_sender.last_name = datos.Tables(0).Rows(Index - 1).Item("last_name").ToString
                    Factura.secondary_sender.commercial_name = datos.Tables(0).Rows(Index - 1).Item("commercial_name").ToString
                    Factura.secondary_sender.initials = datos.Tables(0).Rows(Index - 1).Item("initials").ToString
                    Factura.secondary_sender.tributary_condition = datos.Tables(0).Rows(Index - 1).Item("tributary_condition").ToString
                    Factura.secondary_sender.statal_entity = datos.Tables(0).Rows(Index - 1).Item("statal_entity").ToString
                    Factura.secondary_sender.consortium = datos.Tables(0).Rows(Index - 1).Item("consortium").ToString
                    Factura.secondary_sender.invoices_printer_nit = datos.Tables(0).Rows(Index - 1).Item("invoices_printer_nit").ToString
                    Factura.secondary_sender.invoices_printer_social_reason = datos.Tables(0).Rows(Index - 1).Item("invoices_printer_social_reason").ToString
                    Factura.secondary_sender.aggregation_code = datos.Tables(0).Rows(Index - 1).Item("aggregation_code").ToString
                    Factura.secondary_sender.phone = datos.Tables(0).Rows(Index - 1).Item("phone").ToString
                    Factura.secondary_sender.email = datos.Tables(0).Rows(Index - 1).Item("email").ToString
                    Factura.secondary_sender.address = datos.Tables(0).Rows(Index - 1).Item("address").ToString
                    Factura.secondary_sender.city_code = datos.Tables(0).Rows(Index - 1).Item("city_code").ToString
                    Factura.secondary_sender.country_code = datos.Tables(0).Rows(Index - 1).Item("country_code").ToString
                    Factura.secondary_sender.use_incentives = datos.Tables(0).Rows(Index - 1).Item("use_incentives").ToString

                    Dim productos = objDatos.ConsultarProductosFacturas(datos.Tables(0).Rows(Index - 1).Item("tipo").ToString, datos.Tables(0).Rows(Index - 1).Item("nro").ToString)
                    Factura.products = New List(Of products)
                    If productos.Tables.Count > 0 Then
                        If productos.Tables(0).Rows.Count > 0 Then

                            For indexp = 1 To productos.Tables(0).Rows.Count
                                Dim product As New products
                                product.product = productos.Tables(0).Rows(indexp - 1).Item("product").ToString
                                product.quantity = productos.Tables(0).Rows(indexp - 1).Item("quantity")
                                product.tax = productos.Tables(0).Rows(indexp - 1).Item("tax").ToString
                                product.total_price = productos.Tables(0).Rows(indexp - 1).Item("total_price").ToString
                                product.unitary_price = productos.Tables(0).Rows(indexp - 1).Item("unitary_price")
                                Factura.products.Add(product)
                            Next

                        End If
                    End If

                    Factura.total_value = datos.Tables(0).Rows(Index - 1).Item("total_value").ToString
                    Factura.end_value = datos.Tables(0).Rows(Index - 1).Item("end_value").ToString
                    Factura.delivery_city_code = datos.Tables(0).Rows(Index - 1).Item("delivery_city_code").ToString
                    Factura.delivery_country_code = datos.Tables(0).Rows(Index - 1).Item("delivery_country_code").ToString
                    Factura.payment_method = datos.Tables(0).Rows(Index - 1).Item("payment_method").ToString
                    Factura.delivery_warehouse = datos.Tables(0).Rows(Index - 1).Item("delivery_warehouse").ToString

                    ConsultaOperaciones(Factura)
                    Factura = New FacturaFaktur
                Next


            End If

        End If



    End Function


    Public Sub ConsultaOperaciones(ByVal Factura As FacturaFaktur)
        Dim output = String.Empty
        Dim respuesta As String = ""
        output = JsonConvert.SerializeObject(Factura)
        Dim bytesCodificados As Byte() = System.Text.Encoding.UTF8.GetBytes(output)
        Dim requestjs As HttpWebRequest = WebRequest.Create("https://faktur.co/api/operations/")
        requestjs.Method = "POST"
        'add headers
        requestjs.ContentType = "application/json;charset=utf-8"
        requestjs.Headers.Add("Authorization", "Token 4d53e5ec57632c54e20fbd2514e3bde5536dc3e6")
        'add body
        ServicePointManager.Expect100Continue = True
        ServicePointManager.ServerCertificateValidationCallback = Function() True

        Dim stream As Stream = requestjs.GetRequestStream()
        Dim buffer As Byte() = System.Text.Encoding.UTF8.GetBytes(output)
        stream.Write(buffer, 0, buffer.Length)

        Try
            Dim myHttpWebResponse As HttpWebResponse = requestjs.GetResponse()
            Dim reader As StreamReader = New StreamReader(myHttpWebResponse.GetResponseStream())
            respuesta = respuesta + reader.ReadToEnd()
            'MsgBox(respuesta)
            myHttpWebResponse.Close()
        Catch ex As WebException
            Dim stream1 As Stream = ex.Response.GetResponseStream()
            Dim reader1 As StreamReader = New StreamReader(stream1)
            respuesta = respuesta + reader1.ReadToEnd()
            'MsgBox(respuesta)
            AlmacenarLogProceso(Factura.operation_id.ToString + " " + respuesta)
            'enviado = 0
        End Try

    End Sub

    Public Sub AlmacenarLogProceso(ByVal Log As String)
        Dim Fecha As String = Date.Now.ToString("dd-MM-yyy HH:mm:ss")
        Dim LogFinal As String = Fecha & " - " & Log
        Dim FileLogConsImp As StreamWriter
        FileLogConsImp = My.Computer.FileSystem.OpenTextFileWriter("C:\Users\Public\Documents\LOG_" + Date.Now.ToString("dd-MM-yyyy") + ".txt", True)
        FileLogConsImp.WriteLine(LogFinal)
        FileLogConsImp.Close()
    End Sub

    Public Sub EnvioFactura()
        Dim respuesta As String = ""
        Dim output As String = JsonConvert.SerializeObject("")
        Dim bytesCodificados As Byte() = System.Text.Encoding.UTF8.GetBytes(output)

        Dim requestjs As HttpWebRequest = WebRequest.Create("https://develop.faktur.co/api/operations/")
        requestjs.Method = "GET"
        'add headers
        requestjs.ContentType = "application/json;charset=utf-8"
        requestjs.Headers.Add("Authorization", "Token 4734e6ef93737df5854998613d34e0541557a87f")
        'add body
        ServicePointManager.Expect100Continue = True
        'Dim stream As Stream = requestjs.GetRequestStream()
        'Dim buffer As Byte() = System.Text.Encoding.UTF8.GetBytes(output)
        'stream.Write(buffer, 0, buffer.Length)
        ''send request
        ''AlmacenarLog("crea objeto de HttpWebResponse")
        'Dim responseJS As HttpWebResponse = requestjs.GetResponse()
        ''AlmacenarLog("Va enviar json de factura")
        'Dim reader As StreamReader = New StreamReader(responseJS.GetResponseStream)
        'html = reader.ReadToEnd()
        'AlmacenarLog("Respuesta dian: " + html)
        'fin consumo REST-------------------------------------------------------------------------------------------------

        Try
            Dim myHttpWebResponse As HttpWebResponse = requestjs.GetResponse()
            Dim reader As StreamReader = New StreamReader(myHttpWebResponse.GetResponseStream())
            respuesta = respuesta + reader.ReadToEnd()
            myHttpWebResponse.Close()
        Catch ex As WebException
            Dim stream1 As Stream = ex.Response.GetResponseStream()
            Dim reader1 As StreamReader = New StreamReader(stream1)
            'respuesta = respuesta + reader1.ReadToEnd()
            'enviado = 0
        End Try

    End Sub



End Class
